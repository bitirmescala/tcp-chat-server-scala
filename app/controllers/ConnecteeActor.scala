package controllers

import akka.actor.Actor
import akka.io.Tcp._
import akka.util.ByteString
import akka.actor.ActorRef
import akka.actor.Props

class ConnecteeActor(connection: ActorRef) extends Actor {
  var name: Option[String] = None
  connection ! Write(ByteString("Choose a name: "))
  def receive = {
    case ChatMessage(name, msg) => connection ! Write(ByteString(name + ": " + msg+ "\n"))

    case Received(data) => name match {
      case None =>
        val nameStr = data.utf8String.replaceAllLiterally("\n", "")
        name = Some(nameStr)
        context.parent ! ClientConnected(Connectee(self, nameStr))

      case Some(nameStr) =>
        context.parent ! ChatMessage(nameStr, data.utf8String.dropRight(1))
    }
    case _: ConnectionClosed => 
      context.parent ! ClientDisconnected(Connectee(self, name.getOrElse(":((")))
      context stop self
  }
}

object ConnecteeActor {
  def props(connection: ActorRef) = Props(new ConnecteeActor(connection))
}