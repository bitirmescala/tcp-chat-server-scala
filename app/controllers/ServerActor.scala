package controllers

import akka.actor.Actor
import akka.actor.Props
import akka.actor.ActorRef
import scala.collection.mutable.SortedSet
import akka.util.ByteString
import scala.collection.mutable.HashSet

case class ClientConnected(connectee: Connectee)
case class ClientDisconnected(connectee: Connectee)
case class Connectee(ref: ActorRef, name: String)
case class ChatMessage(sender: String, msg: String)

class ServerActor extends Actor {
  import akka.io.Tcp._
  import context.system

  val openConnections = HashSet[Connectee]()

  def receive = {
    case b @ Bound(localAddress) =>
    // do some logging or setup ...

    case CommandFailed(_: Bind)  => context stop self

    case c @ Connected(remote, local) =>
      val connection = sender()
      val handler = context.actorOf(ConnecteeActor.props(connection))
      connection ! Register(handler)

    case ClientConnected(connectee) =>
      val usersConcat = if (openConnections.size == 0) "nodoby :(" else openConnections.map(_.name).mkString(", ")
      connectee.ref ! ChatMessage("SERVER", "Welcome " + connectee.name + ". Connected users: " + usersConcat)
      
      for (conn <- openConnections)
        conn.ref ! ChatMessage("SERVER", connectee.name + " has joined.")
        
      openConnections += connectee

    case ClientDisconnected(connectee) =>
      openConnections -= connectee
      for (conn <- openConnections)
        conn.ref ! ChatMessage("SERVER", connectee.name + " has left.")

    case m: ChatMessage =>
      for (conn <- openConnections)
        conn.ref ! m

  }

}