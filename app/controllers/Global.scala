package controllers

import play.api._
import akka.actor.{ Actor, ActorRef, Props }
import akka.io.{ IO, Tcp }
import akka.util.ByteString
import java.net.InetSocketAddress
import play.api.libs.concurrent.Akka
import akka.io.Tcp.Bind

object Global extends GlobalSettings {
  var server:ActorRef = null;
  override def onStart(app: Application) {
    implicit val sys = Akka.system(app)
    val manager = IO(Tcp)
    server = sys.actorOf(Props[ServerActor], "ServerActor")
    manager ! Bind(server, new InetSocketAddress(6226))
  }
}